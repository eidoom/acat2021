#!/usr/bin/env python3

import datetime
import pathlib
import shutil
import re

import jinja2
import mistletoe
import strictyaml

# don't match html lines `<...`
p_double_quotes = re.compile(r"(^<.*)\"(.*?)\"")
p_apostrophe_singular = re.compile(r"(\w)\'s")
p_apostrophe_plural = re.compile(r"(\w)s\'")
p_single_quotes = re.compile(r"\'(.*?)\'")


def load_slide(entry):
    with open(entry, "r") as f:
        raw = f.read()

    if raw[:3] != "---":
        raise Exception(f"Format of file {entry} unrecognised")

    metadata, content = raw.split("---", maxsplit=2)[1:]

    data = strictyaml.load(metadata).data

    content = re.sub(p_double_quotes, r"\1“\2”", content)
    content = re.sub(p_apostrophe_singular, r"\1’s", content)
    # this substitution will cause the next to fail if it appears inside a quote using single inverted commas
    content = re.sub(p_apostrophe_plural, r"\1s’", content)
    content = re.sub(p_single_quotes, r"‘\1’", content)
    content = (
        content
        # .translate(str.maketrans("'", "’"))
        .replace("---", "—").replace("--", "–")
    )

    data["content"] = mistletoe.markdown(content).strip()

    return data


if __name__ == "__main__":
    content = pathlib.Path("content")
    layout = pathlib.Path("layout")
    static = pathlib.Path("static")

    for directory in (content, layout, static):
        if not directory.is_dir():
            raise Exception(f"Directory {directory} not found")

    public = pathlib.Path("public")
    public.mkdir(exist_ok=True)

    env = jinja2.Environment(loader=jinja2.FileSystemLoader(layout))

    template_style = env.get_template("style.css.jinja2")

    template = {}
    template["body"] = env.get_template("body.html.jinja2")
    template["title"] = env.get_template("title.html.jinja2")

    for file in static.iterdir():
        shutil.copy(file, public)

    with open("head.yaml", "r") as f:
        head = strictyaml.load(f.read()).data

    head["date"] = (
        datetime.datetime.fromisoformat(head["date"]).date().strftime("%d %b %Y")
    )

    html_style = template_style.render(head)
    with open(public / "style.css", "w") as f:
        f.write(html_style)

    title = {}
    title["head"] = head
    title["index"] = 0
    title["url"] = "index.html"

    slides = []
    for slide in content.iterdir():
        subs = load_slide(slide)
        subs["head"] = head

        if "start" not in subs:
            subs["start"] = False

        subs["url"] = slide.stem + ".html"

        if "next" not in subs:
            subs["next"] = None

        if "layout" not in subs:
            subs["layout"] = "body"

        slides.append(subs)

    title["total"] = len(slides)

    next_slide = next(x for x in slides if x["start"])
    next_slide["prev_url"] = title["url"]
    title["next"] = next_slide["id"]
    title["next_url"] = next_slide["url"]

    i = 1
    while True:
        next_slide["index"] = i
        i += 1
        next_id = next_slide["next"]
        if next_id is None:
            break
        next_slide = next(x for x in slides if x["id"] == next_id)

    html = template["title"].render(title)
    with open(public / title["url"], "w") as f:
        f.write(html)

    for slide in slides:
        slide["total"] = len(slides)

        if slide["next"] is not None:
            slide["next_url"] = next(
                x["url"] for x in slides if x["id"] == slide["next"]
            )

        if not slide["start"]:
            slide["prev_url"] = next(
                x["url"] for x in slides if x["next"] == slide["id"]
            )

        html = template[slide["layout"]].render(slide)

        with open(public / slide["url"], "w") as f:
            f.write(html)
