---
title: Per-point agreement between ANN and NJet
id: points
next: dist
---
<div class="grid lrr">

* Gaussian error distribution, but shifted mean
* IR regions difficult to fit
* Gluon PDF suppresses these errors
* Total cross section in agreement
    * $$ \sigma_\mathrm{NN} = (4.5 \pm 0.6) \times 10^{-6} \thinspace \mathrm{pb} $$
    * $$ \sigma_\mathrm{NJet} = (4.9 \pm 0.5) \times 10^{-6} \thinspace \mathrm{pb} $$
* Improved tree-level descriptions using factorisation
    * [Talk by Truong (Mon)](https://indico.cern.ch/event/855454/contributions/4606380/)
    * [[Maître, Truong arXiv:2107.06625]](https://arxiv.org/pdf/2107.06625)

<div class="image" id="unit_error_plot"></div>

<div class="image" id="x_2_error_plot"></div>

</div>
