---
title: Neural network architecture
id: nn-arch
next: event-generators
---
<div class="grid two-columns">

* General architecture: ensemble, process, cuts
    * Hyperparameter optimisation on $gg\rightarrow\gamma\gamma g$
* Fully-connected neural network
    * Keras with TensorFlow backend
    * $ n \times 4 $ input nodes
    * 20-40-20 hidden nodes (hyperbolic-tangent)
    * Single output node (linear)
* Mean squared error loss function
* Adam optimisation
* Training epochs by Early Stopping

<div id="nn" class="image"></div>

</div>
