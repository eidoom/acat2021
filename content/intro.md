---
title: Motivation
id: intro
next: diphoton
---
<div class="grid two-columns">

* High-multiplicity scattering at colliders
    * Important measurement ingredient
    * Theoretical challenge: algebraic complexity
    * [Talk by Butter (Monday)](https://indico.cern.ch/event/855454/contributions/4628536/)

<div id="jets" class="image">

[[CMS]](https://cds.cern.ch/record/2114784?ln=en)

</div>

</div>
