---
title: Interfacing with event generators
id: event-generators
next: points
---

<div class="center">

* Model training used `Python` libraries
    * Ensemble of 20 models
        * Different random weight initialisation
        * Shuffled data sets
        * Mean and standard error (precision/optimality errors)
* Event generators in `C++`
* Wrote `C++` ensemble inference code
    * Weights of trained networks exported to file
    * `Eigen` used for fast linear algebra
* Custom `C++` interface with `Sherpa`
    * Provides matrix element from `NJet` or ANN model given phase-space point
* `Rivet` used for analysis

</div>
