---
title: Data generation
id: nn-data
next: nn-arch
---

<div class="center">

* Extract from integrator after cuts
    * Phase-space point
    * Matrix element
* First run
    * 100k points
    * 4:1 training and validation sets
* Second run
    * 3M points
    * Testing with different random number seed
* Standardise input and output variable distributions
    * Mean of zero
    * Standard deviation of one
* Test on various cuts

</div>
