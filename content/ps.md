---
title: Phase-space partitioning
id: phase-space
next: nn-data
---
<div class="grid two-columns">

* IR divergences from soft $ s_i $ and collinear $ c_{ij} $ emissions
* Naive model struggles to fit whole phase space
* Partition phase space into <span class="red">$ R_{\text{div}} $</span> and $ R_{\text{non-div}} $
    * Cut on $ \text{min}(s_{ij}/s_{12}) $
* Sub-divide <span class="red">$ R_{\text{div}} $</span> using FKS subtraction
    * Partition functions smoothly isolate singularities
    * $ P_{\text{FKS}} = \left\lbrace (i,j) \mid s_i \lor s_j \lor c_{ij} \right\rbrace $
    * $ S_{ij} = 1 / \left( s_{ij} \sum_{j,k\in P_{\text{FKS}}} \frac{1}{s_{jk}} \right) $
    * $ \lvert\mathcal{A}\rvert^2 = \sum_{i,j} S_{ij} \lvert\mathcal{A}\rvert^2 $
* Weighted network for each $ S_{ij} $
* [[Aylett-Bullock, Badger arXiv:2002.07516]](https://arxiv.org/pdf/2002.07516.pdf)

<div id="ps-partition" class="image"></div>

</div>
