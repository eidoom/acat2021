---
title: Outline
id: outline
next: intro
start: true
---
<div class="center">

* Motivation
* Approximate matrix elements with neural networks
    * $ g g \to \gamma \gamma + \textrm{jets}$
    * Tame infrared (IR) behaviour
    * Train models using amplitude libraries ([OpenLoops](https://openloops.hepforge.org/), [MadLoop](http://madgraph.physics.illinois.edu/), ...)
    * Apply in realistic hadronic collider simulations
* Compare results

</div>
