---
title: Conclusion
id: conc
---

<div class="grid two-columns">

<div>

* Extend pioneering work on ANNs to full hadronic collider simulations
    * Loop-induced $ gg \to \gamma\gamma + \text{jets} $
    * Trained models on FKS partitioned phase space
    * Provide general interface to `Sherpa`
* *ANNs provide general framework for optimising high-multiplicity observables*
    * Excellent agreement in distributions
    * Simulation speed-up: $ N_{\text{infer}} / N_{\text{train}} $
* [[Aylett-Bullock, Badger, Moodie arXiv:2106.09474]](https://arxiv.org/pdf/2106.09474.pdf)
* [[GitLab:`JosephPB/n3jet_diphoton`]](https://gitlab.com/JosephPB/n3jet_diphoton)

</div>

<div class="grid two-columns fill">
    <div class="image" id="deta_yy"></div>
    <div class="image" id="dm_yy"></div>
    <div class="image" id="dphi_jj"></div>
    <div class="image" id="dpt_j2"></div>
</div>

</div>
