---
title: Gluon-initiated diphoton amplitudes
id: diphoton
next: phase-space
---
<div class="grid lrR">

* Loop-induced 
    * Challenge conventional event generator optimisations
* Use amplitudes from [`NJet`](https://bitbucket.org/njet/njet) `C++` library [[Badger et al. arXiv:2106.08664]](https://arxiv.org/pdf/2106.08664.pdf)
* Numerical
    * Integrand reduction, generalised unitarity
    * Automated but slow
* Analytical
    * Finite field reconstruction
        * Talks by [De Laurentis (Monday)](https://indico.cern.ch/event/855454/contributions/4606380/), [Krys (next)](https://indico.cern.ch/event/855454/contributions/4606390/)
    * Unavailable for high multiplicity
* Amplitude neural network (ANN)
    * Favourable amplitude evaluation time scaling

<div id="diphoton" class="image"></div>

<div id="timing" class="image"></div>

</div>
