# Abstract

## Title

Optimising simulations for diphoton production at hadron colliders using amplitude neural networks

## Content

Phenomenological studies of high-multiplicity scattering processes at collider experiments present a substantial theoretical challenge and are increasingly important ingredients in experimental measurements.
We investigate the use of neural networks to approximate matrix elements for these processes, studying the case of loop-induced diphoton production through gluon fusion.
We train neural network models on one-loop amplitudes from the NJet library and interface them with the Sherpa Monte Carlo event generator to provide the matrix element within a realistic hadronic collider simulation.
Computing some standard observables with the models and comparing to conventional techniques, we find excellent agreement in the distributions and a reduced simulation time by a factor of 30.

## Significance

We extend previous work which pioneered the emulation of scattering amplitudes with neural networks, studying these techniques for the first time within a full hadronic collider simulation. 

## References

* https://arxiv.org/abs/2106.09474
* https://doi.org/10.1007/JHEP06(2020)114
