# [ACAT2021](https://gitlab.com/eidoom/acat2021)

* [Live here](https://eidoom.gitlab.io/acat2021/)
* [Event](https://indico.cern.ch/event/855454/)
* [Contribution](https://indico.cern.ch/event/855454/contributions/4606382/)
* [Recording](https://videos.cern.ch/record/2295101)

## Development

```shell
find . -type f ! -path "./.*" | entr ./build.py
```
```shell
live-server public
```
